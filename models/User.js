const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    name: {
         type: String,
          required: true 
        },
    mobile: {
         type : String,
          required: true
        },
    walletBalance: {
        type : String,
        default : '0'
    },
    otp: {
      type: String
    }

});

async function FindUser(mobile){
    try{
      //console.log(req);
      const user = await User.find({ mobile: mobile});
      return (user);
    }
    catch(err){
      return ({message: err});
    }
};

async function PutOtp(mobile, otp){
  try{
    //console.log("id is " + mobile);
    await User.updateOne(
        { "mobile" : mobile },
        { $set: { "otp": otp } });

    const user = await User.find({ mobile: mobile});
    return user;  
    }
  catch(err){
    return ({message: err});
  }
};

async function PutBalance(mobile, balance){
  try{
    //console.log("id is " + mobile);
    await User.updateOne(
        { "mobile" : mobile },
        { $set: { "walletBalance": balance } });

    const user = await User.find({ mobile: mobile});
    return user;  
    }
  catch(err){
    return ({message: err});
  }
};

const User = mongoose.model('Users', UserSchema);

module.exports = {
    PutOtp: PutOtp,
    PutBalance: PutBalance,
    FindUser: FindUser,
    User: User
  };
  