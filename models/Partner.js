const mongoose = require('mongoose');

const PartnerSchema = mongoose.Schema({
    name: {
         type: String,
          required: true 
        },
        apiKey: {
         type : String,
          required: true
        }
});

async function FindPartner(apiKey){
  try{
    //console.log(req);
    const partner = await Partner.find({ apiKey: apiKey});
    return (partner);
  }
  catch(err){
    return ({message: err});
  }
};

const Partner = mongoose.model('Partners', PartnerSchema);

module.exports = {
  FindPartner: FindPartner,
  Partner: Partner
};
