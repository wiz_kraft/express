const express = require('express');
const http = require('http');
const request = require('request');

const app = express();
app.use(express.json());

const router = express.Router();
const mongoose = require('mongoose');
//require('dotenv/config');

const usersRoute = require('./routes/users');
const usersModel = require('./models/User');
const partnersRoute = require('./routes/partners');
const partnersModel = require('./models/Partner');

app.use('/users', usersRoute);
app.use('/partners', partnersRoute);

//PLEASE MAKE SURE THAT "connected to DB" shows up on the console.
//IF THE PROGRAM IS FAILING, CONTACT Abhinav. (your ip might not have been whitelisted).
mongoose.connect(
    'mongodb+srv://wizkraft:dumbPwd@cluster0-xkt5o.mongodb.net/test?retryWrites=true&w=majority',
    {useNewUrlParser: true, useUnifiedTopology: true}, () => {
    console.log('connected to DB!');
});

// LISTENING
const port = process.env.PORT || 3000;
app.listen(port, () => console.log('listening'));

////////////////////////////////////////////////////////////////////////////////

const Helper = require('./helper');
const helper =  new Helper();

app.post('/otp', async (req, res) => {
    try{
        const { apiKey, mobile, amount} = req.body;

        var partner = await partnersModel.FindPartner(apiKey);
        var user = await usersModel.FindUser(mobile);

        helper.ValidatePartner(partner);
        helper.ValidateUser(user, amount);

        //console.log(partner);
        var otp = helper.GenerateOtp(mobile);
        var result = await usersModel.PutOtp(user[0].mobile, otp);
        //console.log(result);
        res.send({
            success: true,
            otp: otp
        });
    }

    catch (err) {
        res.send({
            success: false,
            reason: err.message
        });
    }
});


app.post('/withdraw', async(req, res) => {
    try{
        const { apiKey, mobile, amount, otp} = req.body;

        var partner = await partnersModel.FindPartner(apiKey);
        var user = await usersModel.FindUser(mobile);

        helper.ValidatePartner(partner);
        helper.ValidateUser(user, amount);
        helper.ValidateOtp(otp, user[0].otp);

        newBalance = user[0].walletBalance - amount;
        var result = await usersModel.PutBalance(user[0].mobile, newBalance);
        res.send({
            success: true,
            walletBalance: newBalance
        });
    }
    catch (err) {
        res.send({
            success: false,
            reason: err.message
        });
    }
});