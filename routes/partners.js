const express = require('express');
const router = express.Router();
const Partner = require('../models/Partner');

//GET ALL PARTNERS
router.get('/', async (req, res) => {
    try{
        const partners = await Partner.find();
        res.json(partners).send();
    }
    catch(err){
        res.json({message: err});
    }
});

//GET SPECIFIC PARTNER
router.get('/:id', async (req, res) =>{
    try{
        //console.log(req);
        const partner = await Partner.findById(req.params.id);
        res.json(partner);
    }
    catch(err){
        res.json({message: err});
    }
});

router.get('/find/:apiKey', async (req, res) => {
    try{
        //console.log(req);
        const partner = await Partner.find({ apiKey: req.params.apiKey});
        res.json(partner);
    }
    catch(err){
        res.json({message: err});
    }
});


//SUBMIT ONE PARTNER
router.post('/', async (req, res) => {
    //console.log(req.body);
    const partner = new Partner({
        name: req.body.name,
        apiKey: req.body.apiKey
    });

    try{
        const savedPartner = await partner.save();
        res.json(savedPartner);
    }
    catch(err){
        res.json({message: err});
    }
});


module.exports = router;