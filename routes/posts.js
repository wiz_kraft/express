const express = require('express');
const router = express.Router();
const Post = require('../models/Post');
const User = require('../models/User');
const Partner = require('../models/Partner');

//ROUTES
//Moved to routes/posts.js





//GET ALL POSTS
router.get('/', async (req, res) => {
    try{
        const posts = await Post.find();
        res.json(posts);
    }
    catch(err){
        res.json({message: err});
    }
});

//GET SPECIFIC POST
router.get('/:id', async (req, res) =>{
    try{
        const post = await Post.findById(req.params.id);
        res.json(post);
    }
    catch(err){
        res.json({message: err});
    }
});


//SUBMIT ONE POST
router.post('/', async (req, res) => {
    //console.log(req.body);
    const post = new Post({
        title: req.body.title,
        description: req.body.description
    });

    try{
        const savedPost = await post.save();
        res.json(savedPost);
    }
    catch(err){
        res.json({message: err});
    }
});

module.exports = router;