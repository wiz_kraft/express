const express = require('express');
const router = express.Router();
const User = require('../models/User');
const Partner = require('../models/Partner');

//GET ALL USERS
router.get('/', async (req, res) => {
    try{
        const users = await User.find();
        res.json(users);
    }
    catch(err){
        res.json({message: err});
    }
});

//GET SPECIFIC USER
router.get('/:id', async (req, res) =>{
    try{
        const user = await User.findById(req.params.id);
        res.json(user);
    }
    catch(err){
        res.json({message: err});
    }
});

router.get('/find/:mobile', async (req, res) => {
    try{
        //console.log(req);
        const user = await User.find({ mobile: req.params.mobile});
        res.json(user);
    }
    catch(err){
        res.json({message: err});
    }
});

//SUBMIT ONE USER
router.post('/', async (req, res) => {
    //console.log(req.body);
    const user = new User({
        name: req.body.name,
        mobile: req.body.mobile
    });

    try{
        const savedUser = await user.save();
        res.json(savedUser);
    }
    catch(err){
        res.json({message: err});
    }
});

module.exports = router;