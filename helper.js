class Helper{
    ValidateUser(user, amount)
    {
        if(isEmptyObject(user))
        {
            throw new Error("User does not exist in our DB");
        }
        if(user.length > 1)
        {
            throw new Error("More than 1 user with the same number");
        }
    
        if(user[0].walletBalance < amount)
        {
            throw new Error("User doesn't have sufficient balance");
        }

        console.log('user validation done.')
    }
    
    ValidatePartner(partner)
    {
        if(isEmptyObject(partner))
        {
            throw new Error("Partner does not exist in our DB");
        }

        console.log('partner validation done.')
    }
    
    GenerateOtp()
    {
        const otp = Math.floor(100000 + Math.random() * (999999 - 100000));
        return otp;
    }
    
    ValidateOtp(a,b)
    {
        if(a!=b)
        {
            throw new Error("OTPs do not match");
        }
    }
}

function isEmptyObject(obj) {
    return !Object.keys(obj).length;
}

module.exports = Helper;